@extends('layouts.apps')

@section('content')
<div class="container">
    <h5 >QUI SOMMES-NOUS ?</h5>
    <style>
        h1 {text-align: center;}
        p {text-align: center;}
        div {text-align: center;}
        </style>
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-8">
              <div class="">
                <div class="">
                  
                  <p class="">
             
                    La société TMI AG est une société familiale fondée en 1984 par Thierry Hermann et est spécialisé dans les solutions d’accessibilité à installer sur des escaliers existants.


                  
                  <p>
                    Notre secteur d’activité est principalement le transport de personnes âgées et/ou handicapées via une gamme complète de monte-escaliers (www.suisse-monte-escaliers.ch) 
                    et depuis 2019 nous avons ouvert une division Monte-charges utilisant la même technologie que nos monte-escaliers.

                  </p>
                  <p>
                    Le produit EasyLift™ a été breveté et nous le distribuons dans tous les pays de la zone UE. Nous disposons d’un réseau de techniciens locaux partout en Europe. 
                    Depuis 2015, la direction de l’entreprise a été confié à Romain Hermann, troisième fils du fondateur.
                    Notre siège social est basé à Bâle (CH) et nous disposons d’une agence en suisse romande à Genève.
                  </p>
                   
                    <p>
                        Siège Social :
                    TMI AG
                    Sankt-Alban Anlage 46
                    CH-4057 Basel
                    Tel : +41 61 270 21 21
                    info@schweiz-treppenlifte.ch

                    </p>
                    
                    <p>
                        Agence de Genève :
                        TMI AG
                        41A, route des jeunes 
                        CP 215
                        CH-1233 Bernex
                        Tel : +41 22 566 86 17
                        info@suisse-monte-escaliers.ch 
                    </p>
                   
                    
                    </p>
                  
                </div>
              </div>
            </div>
            <div class="col-sm-4">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="img/core-img/logo.jpg" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">
                        Thierry Hermann, désormais à la retraite, un dimanche soir avec sa petite fille Zoé</p>
                    </div>
                  </div>
                  <br>
                  <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="img/core-img/logo.jpg" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">
                        Romain Hermann, Technicien dans l’âme, validant l’installation d’une plateforme à Genève</p>
                    </div>
                  </div>


            </div>
          </div>
    </div>
</div>
@endsection
