<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Easy </title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Style CSS -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/animare.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="scss/_mixin.scss">
    <link rel="stylesheet" href="scss/_responsive.scss">
    <link rel="stylesheet" href="scss/_theme_color.scss">
    <link rel="stylesheet" href="scss/_variables.scss">
   

    <!-- style font -->
    <link rel="stylesheet" href="fonts/fontawesome-webfont.eot">
    <link rel="stylesheet" href="fonts/fontawesome-webfont.svg">
    <link rel="stylesheet" href="fonts/fontawesome-webfont.ttf">
    <link rel="stylesheet" href="fonts/fontawesome-webfont.wolf">
    <link rel="stylesheet" href="fonts/fontawesome-webfont.wolf2">
    <link rel="stylesheet" href="fonts/themify.eot">
    <link rel="stylesheet" href="fonts/themify.svg">
    <link rel="stylesheet" href="fonts/themify.tff">
    <link rel="stylesheet" href="fonts/themiy.wolf">

   
  </head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <!-- Logo -->
                        <a class="navbar-brand" href="index.html"><img src="img/core-img/logo.jpg" width="80px" alt="Logo"></a>
                        <!-- Navbar Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Navbar -->
                        <div class="collapse navbar-collapse" id="worldNav" id="myTab" role="tablist">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"
                                    id="home-tab" data-toggle="tab" href="#home" role="tab" 
                                    
                                    >Home <span class="sr-only">(current)</span></a>
                                </li>
                               
                                <li class="nav-item">
                                    <a class="nav-link" href="/produit">Produit & tarifs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/devis">Devis immédiat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/service">Service</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/realisation">Derniéres réealisations</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="/Qui-Sommes-nous    ">Qui sommes-nous ?</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="/contact">Contact</a>
                            </li>
                            </ul>
                            <!-- Search Form  -->
                           
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ********** Hero Area Start ********** -->
    <div class="hero-area">

        <!-- Hero Slides Area -->
        <div class="hero-slides owl-carousel">
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay"
             style="background-image: url(img/blog-img/bg2.jpg);"></div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" 
            style="background-image: url(img/blog-img/bg1.jpg);"></div>

            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" 
            style="background-image: url(img/blog-img/bg3.jpg);"></div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" 
            style="background-image: url(img/blog-img/bg4.jpg);"></div>
        </div>

        <!-- Hero Post Slide -->
        <div class="hero-post-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-post-slide">
                            <!-- Single Slide -->
                            <div class="single-slide d-flex align-items-center">
                                <div class="post-number">
                                    <p>1</p>
                                </div>
                                <div class="post-title">
                                    <a href="single-blog.html">
                                      Vous devez porter des marchandises lourdes et encombrantes ? (logo Easy-lift) est fait pour vous, voyez plutôt
                                    </a>
                                </div>
                            </div>
                            <!-- Single Slide -->
                            <div class="single-slide d-flex align-items-center">
                                <div class="post-number">
                                    <p>2</p>
                                </div>
                                <div class="post-title">
                                    <a href="single-blog.html">photo</a>
                                </div>
                            </div>
                            <!-- Single Slide -->
                            <div class="single-slide d-flex align-items-center">
                                <div class="post-number">
                                    <p>3</p>
                                </div>
                                <div class="post-title">
                                    <a href="single-blog.html">
                                      « Notre système s’installe facilement sur les marches existantes, aucun gros travaux, juste quelques visses sur vos marches, 
                                      installations en moins de 4 heures…. 
                                    </a>
                                </div>
                            </div>
                            <!-- Single Slide -->
                            <div class="single-slide d-flex align-items-center">
                                <div class="post-number">
                                    <p>4</p>
                                </div>
                                <div class="post-title">
                                    <a href="single-blog.html">
                                      Photo 14
                                    </a>
                                </div>
                            </div>
                            <div class="single-slide d-flex align-items-center">
                              <div class="post-number">
                                  <p>5</p>
                              </div>
                              <div class="post-title">
                                  <a href="single-blog.html">
                                    « (logo Easy-lift) peut transporter jusqu’à 160KG et s’adapte à toutes marchandises,
                                     vous pouvez choisir le revêtement du plateau en fonction de vos exigences….
                                  </a>
                              </div>
                          </div>


                          <div class="single-slide d-flex align-items-center">
                            <div class="post-number">
                                <p>6</p>
                            </div>
                            <div class="post-title">
                                <a href="single-blog.html">
                                  Photo 15
                                </a>
                            </div>
                        </div>
                        <div class="single-slide d-flex align-items-center">
                          <div class="post-number">
                              <p>7</p>
                          </div>
                          <div class="post-title">
                              <a href="single-blog.html">
                              « Demandez 
                                votre devis instantané via l’onglet « Devis immédiat» ou bien contactez-nous pour obtenir plus d’informations ! »
                              </a>
                          </div>
                      </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ********** Hero Area End ********** -->













    <div class="main-content-wrapper section-padding-100">
        <div class="container">
        


      
            <main class="py-4">
                @yield('content')
            </main>
         

            

            <!-- Load More btn -->
          
        </div>
    </div>

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                        <a href="#"><img src="img/core-img/logo.png" alt=""></a>
                        <div class="copywrite-text mt-30">
                            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                        <ul class="footer-menu d-flex justify-content-between">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Fashion</a></li>
                            <li><a href="#">Lifestyle</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Gadgets</a></li>
                            <li><a href="#">Video</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                        <h5>Subscribe</h5>
                        <form action="#" method="post">
                            <input type="email" name="email" id="email" placeholder="Enter your mail">
                            <button type="button"><i class="fa fa-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>

</html>