@extends('layouts.apps')

@section('content')
<div class="container">
    
    <style>
        h1 {text-align: center;}
        p {text-align: center;}
        div {text-align: center;}
        </style>
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-6">
              <div class="">
                <div class="">
                  <p>
                    Nous mettons tout en œuvre pour pouvoir vous fournir votre monte-charge Easy-Lift™ le plus facilement possible.

                  </p>
                  <p>
                    Nous avons tout mis en œuvre pour que le processus, allant de la prise de contact à l’installation soit le plus simple possible.

                  </p>
                  <p>
                    Nous utilisons au maximum Internet et pouvons ainsi optimiser le processus de commande. Bien évidemment nos équipes sont disponibles en cas de besoin.

                  </p>
                   <p>
                    Ci-dessous un résumé de nos services :
                       </p> 
                       <p>Demande de documentations </p>
                    
                   <p> Si vous souhaitez une documentation commerciale et technique,
                    vous pouvez l’obtenir immédiatement en allant dans l’onglet Contact</p>
                 
                 <p> Demande de devis </p>
                   <p>Si vous souhaitez un devis précis, vous pouvez l’obtenir en allant dans l’onglet Devis immédiat, ce service vous permet d’obtenir un devis par vous-même de manière immédiate, il vous suffit de renseigner 4 informations très faciles à obtenir et de télécharger 3 photos. Avec ce service, pas besoin de discuter, d’attendre c’est direct. 
                    Demande de prix </p>
                    <p>Vous pouvez consulter nos prix dans l’onglet Produit & Tarifs, nos tarifs sont simples et sans vices cachés.
                        Installation de votre monte-charge </p>
                    <p>Nos équipes sont locales et n’attendent qu’à venir vous installer votre appareil, il faut juste commander !!
                        Service après-vente</p>
                    <p>
                        Nous assurons vos dépannages et révisions via nos équipes locales, rapides et efficaces 
                        Autres demandes 
                    </p>
                   <p> Pour toutes autres demandes notre service client est à votre écoute sur l’email info@easy-lift-go.com 
                    NB : tous les liens en bleus, en cliquant dessus doivent rediriger le site sur la page en question, pour le lien info@easy-lift.com quand on clique dessus on doit arriver sur la page « CONTACT »</p>
                   
                    
              
                </div>
              </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="" style="width: 18rem;">
                            <img class="card-img-top" src="img/core-img/logo.jpg"
                             >
                            <div class="">
                              <p class="">
                                Nous vous proposons d’établir vous-même votre devis, c’est très simple et rapide</p>
                            </div>
                          </div>
                    </div>
                    <br>
                    <div class="col-sm-6">
                        <div class="" style="width: 18rem;">
                            <img class="card-img-top" src="img/core-img/logo.jpg"
                             >
                            <div class="card-body">
                              <p class="card-text">
                                Nous livrons votre appareil directement sur le chantier depuis notre usine                            </div>
                          </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-sm-6">
                        <div class="" style="width: 18rem;">
                            <img class="card-img-top" src="img/core-img/logo.jpg"
                             >
                            <div class="card-body">
                              <p class="card-text">
                                Ou que vous soyez en Europe, ce sera une équipe locale qui interviendra, SAV fiable et rapide                            </div>
                          </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="" style="width: 18rem; bordure:2px">
                            <img class="card-img-top" src="img/core-img/logo.jpg"
                             >
                            <div class="card-body">
                              <p class="card-text">
                                Tous nos appareils sont garantis 2ans pièces et main d’œuvre                            </div>
                          </div>
                    </div>
                  </div>


            </div>
          </div>
    </div>
</div>
@endsection
