@extends('layouts.apps')

@section('content')
<div class="container">
    
    <style>
        h1 {text-align: center;}
        p {text-align: center;}
        div {text-align: center;}
        </style>
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-8">
              <div class="">
                <div class="">
                  
             <p>
                Bonjour, bienvenue dans votre interface qui vous permet d’obtenir votre devis directement via email.
                
             </p>
             <p>
                Pour ce faire nous avons besoin de certaines informations techniques, très simples à calculer, ainsi que 3 photos. Afin que vous puissiez renseigner votre dossier, très rapide, vous devez créer un compte, ce qui vous permettra de conserver toutes vos informations et vous laissera le temps d’aller calculer les mesures nécessaires et de prendre les photos demandées. Pour créer le compte seule votre adresse email suffit, c’est cette adresse email qui vous servira d’identifiant, pas de mot de passe requis. Vous pourrez ainsi revenir compléter votre dossier à tout moment pendant 7 jours. Une fois que nous aurons réceptionné toutes les informations requises, vous obtiendrez votre devis par email et il sera disponible dans votre interface client pendant 60 jours.
             </p>
                Creéz votre émail 
                <form>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                      </div>
                    </div>
                </form>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="img/core-img/logo.jpg" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">
                        Thierry Hermann, désormais à la retraite, un dimanche soir avec sa petite fille Zoé</p>
                    </div>
                  </div>
                  <br>
                  <div class="card" style="width: 18rem;">
                    <a href="/devis" class="btn btn-danger">
                        voire le video</a>
                   
                  </div>


            </div>
          </div>
    </div>
</div>
@endsection
