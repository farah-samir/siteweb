@extends('layouts.apps')

@section('content')
<div class="container">
    
    <style>
        h1 {text-align: center;}
        p {text-align: center;}
        div {text-align: center;}
        </style>
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-8">
              <div class="">
                <div class="">
                  
               <p>
                Le Monte-Charge Easy-Lift™ est la solution pour le transport de marchandise le long d'un escalier droit ou courbe. Plus besoin de faire des allers-retours pénibles, une simple pression sur un
                des télécommandes suffira à monter ou descendre vos marchandises les plus lourdes ou encombrantes.
               </p>
                  <p>
                    Easy-Lift™ est un système qui se fixe sur les marches existantes, il n’y a pas de gros travaux n’y de modifications de la structure, 
                    seules quelques visses sont fixés sur les marches c’est tout.
                  </p>
                  <p>
                    Easy-Lift™ est un produit breveté sous le numéro 14/57905 en date du20/08/2014 et c’est la société TMI AG qui en a la distribution exclusive.
                  </p>
                  <p>
                    Combien ça coûte ?
                  </p>
                  <p>
                    Pour une installation standard (jusqu’à5 mètres de rail) il faut compter 4’990€ HT, ce prix comprend l’étude de faisabilité, la livraison, la pose, l’explication & mise en service avec une garantie de 2 ans. 
                  </p>
                <p>
                    Quelques options sont disponibles mais n’augmenterons pas le prix de manière conséquente.
                </p>
                <p>
                    Vous souhaitez un devis ?
                </p>
                <a href="/devis" class="btn btn-warning">Devis immédiat</a>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="img/core-img/logo.jpg" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">
                        Thierry Hermann, désormais à la retraite, un dimanche soir avec sa petite fille Zoé</p>
                    </div>
                  </div>
                  <br>
                  <div class="card" style="width: 18rem;">
                    <a href="/devis" class="btn btn-danger">voire le video</a>
                    <br>
                    <a href="/contact" class="btn btn-danger">Obtenir Documentation</a>
                    <br>
                    <a href="/contact" class="btn btn-danger">Demandez à être rappelé </a>
                  </div>


            </div>
          </div>
    </div>
</div>
@endsection
